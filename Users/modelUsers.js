const md5 = require("md5");

const userList = [];

class modelUsers {
    getAllUsers = () => {
        return userList;
    }

    isUserIdentified = (daftarData) => {
        const usedData = userList.find((addedData) => {
        return addedData.username === daftarData.username || 
        addedData.email === daftarData.email;
        });
    
        if (usedData) {
            return true;
          } else {
            return false;
          }
    };

    storeNewUsers = (daftarData) => {
    userList.push({
        id: userList.length + 1,
        username: daftarData.username,
        email: daftarData.email,
        password: md5(daftarData.password),
        });
    };

    verifyLogin = (dataBody) => {
        const dataLogin = userList.find((user) => {
            return user.username === dataBody.username && user.password === md5(dataBody.password);
          });

        return dataLogin;
    };
}


module.exports = new modelUsers;