const express = require ("express");
const controlUsers = require("./controlUsers");
const routeUsers = express.Router();

routeUsers.get("/user", controlUsers.getAllUsers);

routeUsers.post("/daftar", controlUsers.storeNewUsers);

routeUsers.post("/login", controlUsers.verifyLogin);

module.exports = routeUsers;