const modelUsers = require ("./modelUsers");

class controlUsers {
    getAllUsers = (req, res) => {
        const allUsers = modelUsers.getAllUsers();
        return res.json(allUsers);
    };

    storeNewUsers = (req, res) => {
        const daftarData = req.body;
        if (daftarData.username === undefined || daftarData.username === ""){
            res.statusCode = 404;
            return res.json({message: "username is unavailable"});
        }
        if (daftarData.email === undefined || daftarData.email === ""){
          res.statusCode = 404;
          return res.json({message: "email is unavailable"});
        }
        if (daftarData.password === undefined || daftarData.password === ""){
          res.statusCode = 404;
          return res.json({message: "password is unavailable"});
        }

        const usedData = modelUsers.isUserIdentified(daftarData);
        if (usedData) {
            res.statusCode = 404;
            return res.json({message: "username or email already exist"});
        }   

        modelUsers.storeNewUsers(daftarData);

        res.json({ message: "new user is being added!" });
    };

    verifyLogin = (req, res) => {
        const dataBody = req.body;
        const dataLogin = modelUsers.verifyLogin(dataBody);
    
        if (dataLogin === undefined) {
            res.statusCode = 404;
            return res.json({ message: "user not identified!" });
        }
        
        return res.json(dataLogin);
    }; 
}

module.exports = new controlUsers();